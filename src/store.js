import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store  = new Vuex.Store({
    state:{
        items:[],
        deletedItems:[]
    },
    mutations:{
        additem:function(state,text){
            state.items.push({
                id: parseInt(Math.random()*1000),
                text:text,
                checked:false
            });
        },
        updateitem:function(state,item){
            state.items.forEach(element => {
                if(element.id == item.id){
                  element.checked = item.checked;
                }
            });
        },
        deleteitem:function(state,item){
            let i = 0;
            state.items.forEach((val,index) => {
                if(item.id == val.id){
                i = index;
                }
            });
            let deleted = state.items[i];
            state.deletedItems.push(deleted);
            state.items.splice(i,1);
        },
        updatestorage:function(state){
            if(typeof(Storage) !== "undefined"){
                localStorage.setItem("todolist",JSON.stringify(state.items));
                localStorage.setItem("todolist-deleted",JSON.stringify(state.deletedItems));
            }
        },
        loadfromstorage:function(state){
            if(typeof(Storage) !== "undefined"){
                if(localStorage.getItem("todolist") != null){
                  state.items = JSON.parse(localStorage.getItem("todolist"));
                  if(!state.items) state.items = [];
        
                  state.deletedItems = JSON.parse(localStorage.getItem("todolist-deleted"));
                  if(!state.deletedItems) state.deletedItems = [];
                }
            }
        }
    }
});

export default store;