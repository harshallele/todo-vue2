import VueRouter from 'vue-router';
import DefaultView from './components/defaultview.vue';
import DeletedView from './components/deletedview.vue';
import Vue from "vue";

Vue.use(VueRouter);

const router = new VueRouter({
    routes:[
        {path:'/', component:DefaultView},
        {path:'/deleted', component: DeletedView}
    ]
});

export default router;